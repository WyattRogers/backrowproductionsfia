﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class PlayerData
{
    public string playerName;
    public int saveSlotInd;
    public int skinInd;
    public int coins;
    public int health;
    public int lives;
    public int levelUnlocked;
    public string curLevelName;
    public int curCheckPoint;
}

[Serializable]
public class PlayerContainer
{
    public List<PlayerData> players = new List<PlayerData>();
}

[Serializable]
public class SaveData
{
    public int saveSlotInd = 0;
}

public class GameDataManager : MonoBehaviour
{
    private string playerDataPath;
    private string saveDataPath;
    private PlayerContainer playerContainer = new PlayerContainer();

    private SaveData saveData = new SaveData();

    private PlayerData curPlayer = null;
    private string curPlayerName;

    [SerializeField]
    private int defaultHp = 5;
    [SerializeField]
    private int defaultLives = 3;

    // Use this for initialization
    void Start()
    {
        GetDataPaths();
        //LoadSaveData
        StartCoroutine(StartLoadData());
    }

    IEnumerator StartLoadData()
    {
        LoadSaveData();
        while (saveData == null)
        {
            yield return new WaitForEndOfFrame();
        }
        LoadAllSavedPlayers();
    }

    #region PLAYER_DATA_SETUP

    void GetDataPaths()
    {
        if (!Directory.Exists(Application.streamingAssetsPath))
        {
            Debug.Log("Assets/StreamingAssets directory does not exist! Creating one....");
            Directory.CreateDirectory(Application.streamingAssetsPath);
        }
            
        playerDataPath = Path.Combine(Application.streamingAssetsPath, "PlayerData.json");
        saveDataPath = Path.Combine(Application.streamingAssetsPath, "SaveData.json");
    }

    public void SaveNewPlayerToJson(string _playerName, int _id)
    {
        //create new player from player class
        curPlayer = new PlayerData();
        curPlayer.playerName = _playerName;
        curPlayer.saveSlotInd = _id;
        curPlayer.health = defaultHp;
        curPlayer.lives = defaultLives;
        curPlayer.coins = 0;
        curPlayer.lives = 3;
        curPlayer.levelUnlocked = 1;

        //append player to local list
        playerContainer.players.Add(curPlayer);

        //update playerdata
        UpdatePlayerSaveData();
    }

    void LoadSaveData()
    {
        //create a json file
        if (!File.Exists(saveDataPath))
        {
            Debug.Log("No SaveData.Json file found! Creating a new one . . . ");
            UpdateSaveData();
        }
        //get string data from json file
        string saveDataJson = File.ReadAllText(saveDataPath);
        //store all player data into playercontainer
        saveData = JsonUtility.FromJson<SaveData>(saveDataJson);
        if (saveData == null)
        {
            Debug.Log("Could not create SaveData file! Make sure you have an Assets/StreamingAssets folder in your project.");
        }
    }

    void LoadAllSavedPlayers()
    {
        //create a json file and load a player container if playerdata file does not exist
        if (!File.Exists(playerDataPath))
        {
            Debug.Log("No PlayerData.Json file found! Creating a new one . . . ");
            UpdatePlayerSaveData();
        }   
        //get string data form json file
        string playerJson = File.ReadAllText(playerDataPath);
        //store all player data into playercontainer
        playerContainer = JsonUtility.FromJson<PlayerContainer>(playerJson);
        //store curplayer
        curPlayer = GetPlayerDataJson(GetCurSaveSlotInd());
    }

    public void UpdateSaveData()
    {
            
        //store data to json string
        string saveDataJson = JsonUtility.ToJson(saveData);

        //use streamwriter to create text file while playing
        StreamWriter sw = File.CreateText(saveDataPath);
        sw.Close();

        //write json data to file
        File.WriteAllText(saveDataPath, saveDataJson);
    }

    public void UpdatePlayerSaveData()
    {
        //store player data to json string
        string playerJson = JsonUtility.ToJson(playerContainer);

        //use streamwriter to create text file while playing
        StreamWriter sw = File.CreateText(playerDataPath);
        sw.Close();

        //write json data to file
        File.WriteAllText(playerDataPath, playerJson);
    }

    public void SetCurSaveSlotInd(int _ind)
    {
        saveData.saveSlotInd = _ind;
        UpdateSaveData();
    }

    public int GetCurSaveSlotInd()
    {
        return saveData.saveSlotInd;
    }

    public void ErasePlayer(int _saveSlotInd)
    {
        PlayerData player;
        if (PlayerExists(_saveSlotInd, out player))
        {
            playerContainer.players.Remove(player);
            UpdatePlayerSaveData();
        }
        
    }

    public void ClearAllSavedPlayers()
    {
        //clear players from local list
        playerContainer.players.Clear();
        //update json data
        UpdatePlayerSaveData();
    }

    public bool PlayerExists(string playerName)
    {
        foreach (var player in playerContainer.players)
        {
            if (player.playerName == playerName)
            {
                return true;
            }
        }
        return false;
    }

    public bool PlayerExists(string playerName, out PlayerData playerData)
    {
        foreach (var player in playerContainer.players)
        {
            if (player.playerName == playerName)
            {
                playerData = player;
                return true;
            }
        }
        playerData = null;
        return false;
    }

    public bool PlayerExists(int _saveSlotInd, out PlayerData playerData)
    {
        foreach (var player in playerContainer.players)
        {
            if (player.saveSlotInd == _saveSlotInd)
            {
                playerData = player;
                return true;
            }
        }
        playerData = null;
        return false;
    }

    public PlayerData GetPlayerDataJson(int _saveSlotInd)
    {
        foreach (var player in playerContainer.players)
        {
            if (player.saveSlotInd == _saveSlotInd)
            {
                return player;
            }
        }
        Debug.Log("No player found with saveSlotInd: " + _saveSlotInd + ". You need to create a player. " +
            "Make sure you also have an Assets/StreamingAssets folder in your project.");
        return null;
    }

    public void SetCurPlayer(int _saveSlotInd)
    {
        curPlayer = GetPlayerDataJson(_saveSlotInd);
    }

    public PlayerData GetCurPlayer()
    {
        if (curPlayer != null)
            return curPlayer;
        else
            return null;
    }

    public PlayerData LoadPlayerFromJson(string playerName)
    {
        if (PlayerExists(playerName, out curPlayer))
            return curPlayer;
        else
            return null;
    }

    public PlayerContainer GetPlayerContainer()
    {
        return playerContainer;
    }

    public void SetCurPlayerName(string _name)
    {
        curPlayer.playerName = _name;
        UpdatePlayerSaveData();
    }

    #endregion

    #region SKIN

    public void SetCurPlayerSkin(int _curInd)
    {
        curPlayer.skinInd = _curInd;
        UpdatePlayerSaveData();
    }

    public void ResetPlayerSkin()
    {
        curPlayer.skinInd = 0;
        UpdatePlayerSaveData();
    }

    public int GetCurPlayerSkin()
    {
        return curPlayer.skinInd;
    }

    #endregion

    #region COINS

    public void SetCurCoins(int _amount)
    {
        curPlayer.coins = _amount;
        UpdatePlayerSaveData();
    }

    public void ResetCoins()
    {
        curPlayer.coins = 0;
        UpdatePlayerSaveData();
    }

    public int GetCurCoins()
    {
        return curPlayer.coins;
    }

    #endregion

    #region LIVES

    public void SetCurLives(int _curLives)
    {
        curPlayer.lives = _curLives;
        UpdatePlayerSaveData();
    }

    public void ResetLives()
    {
        curPlayer.lives = defaultLives;
        UpdatePlayerSaveData();
    }

    public int GetCurLives()
    {
        return curPlayer.lives;
    }

    #endregion

    #region HEALTH

    public void SetCurHealth(int _curHealth)
    {
        curPlayer.health = _curHealth;
        UpdatePlayerSaveData();
    }

    public void ResetHealth()
    {
        curPlayer.health = defaultHp;
        UpdatePlayerSaveData();
    }

    public int GetCurHealth()
    {
        return curPlayer.health;
    }

    #endregion

    #region LEVELPROGRESS

    public void SetCurUnlockedLevel(int _levelUnlocked)
    {
        if (_levelUnlocked > curPlayer.levelUnlocked)
        {
            Debug.Log("Saving Level Progress to Disc. Build index " + _levelUnlocked + " unlocked.");
            curPlayer.levelUnlocked = _levelUnlocked;
            UpdatePlayerSaveData();
        }
        else
            Debug.Log("Level progress already unlocked to this level");
    }

    public int GetCurProgress()
    {
        return curPlayer.levelUnlocked;
    }

    public void ResetProgress()
    {
        curPlayer.levelUnlocked = 1;
        UpdatePlayerSaveData();
    }

    #endregion

    #region SAVINGCHECKPOINTS

    public void SetCurCheckPoint(int _ind)
    {
        if (curPlayer.curCheckPoint == _ind)
            return;

        curPlayer.curCheckPoint = _ind;
        UpdatePlayerSaveData();
        Debug.Log("Saving current checkpoint: " + curPlayer.curCheckPoint + " to disc.");
    }

    public void ResetCurCheckPoint()
    {
        curPlayer.curCheckPoint = 0;
        UpdatePlayerSaveData();
    }

    public int GetCurSavedCheckPoint()
    {
        return curPlayer.curCheckPoint;
    }

    #endregion

    #region SAVINGLEVELDATA

    public void SetCurLevelName(string _levelName)
    {
        curPlayer.curLevelName = _levelName;
        UpdatePlayerSaveData();
    }

    public void EraseSavedLevelName()
    {
        curPlayer.curLevelName = null;
    }

    public string GetCurSavedLevelName()
    {
        if (curPlayer.curLevelName != null)
        {
            return curPlayer.curLevelName;
        }
        else
        {
            Debug.Log("No Scene name data found. Returning null");
            return null;
        }
    }

    #endregion
}
