﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuLevelManager : MonoBehaviour 
{

    [SerializeField]
    private bool displayAll = false;
    [SerializeField]
    private StartMenuLevel[] levels;

    private GameDataManager dataManager;

	// Use this for initialization
	void Start () 
	{
        GetComponents();

        StartCoroutine(StartWaitForData());   
    }

    void GetComponents()
    {
        //getcomponents
        dataManager = GameManager.instance.GetGameDataManager();
    }

    IEnumerator StartWaitForData()
    {
        while (dataManager.GetCurPlayer() == null)
        {
            yield return new WaitForEndOfFrame();
        }
        if (displayAll)
        {
            dataManager.SetCurUnlockedLevel(levels.Length);
            yield return new WaitForEndOfFrame();
        }
        DisplayPlayableLevels();

    }

    public void RefreshPlayableLevels()
    {
        StartCoroutine(StartRefreshPlayableLevels());
    }

    IEnumerator StartRefreshPlayableLevels()
    {
        yield return new WaitForEndOfFrame();
        DisplayPlayableLevels();
    }

    void DisplayPlayableLevels()
    {

        foreach (var level in levels)
        {
            if (dataManager.GetCurProgress() >= level.GetLevelNumber())
            {
                level.SetLevelPlayable(true);
            }
            else
            {
                level.SetLevelPlayable(false);
            }

        }
    }
	
}
