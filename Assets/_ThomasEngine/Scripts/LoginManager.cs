﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class LoginManager : MonoBehaviour
{
    [SerializeField]
    private GameObject loginPanelPrefab;
    [SerializeField]
    private Transform loginPanelParent;
    [SerializeField]
    private int saveSlots = 3;
    [SerializeField]
    private string nextSceneToPlay;

    private GameDataManager dataManager;
    private PlayerContainer players = new PlayerContainer();

    private List<LoginPanel> loadedPanels = new List<LoginPanel>();

    // Use this for initialization
    void Start()
    {
        dataManager = GameManager.instance.GetGameDataManager();
        RefreshPlayerList();
    }

    IEnumerator StartLoadPlayerContainers()
    {       
        while(dataManager.GetPlayerContainer() == null)
        {
            yield return new WaitForEndOfFrame();
        } 
        players = dataManager.GetPlayerContainer();
        DisplayPlayers();
    }

    public void RefreshPlayerList()
    {
        StartCoroutine(StartLoadPlayerContainers());
    }

    void DisplayPlayers()
    {
        if (loadedPanels.Count == 0)
        {
            for (int i = 0; i < saveSlots; i++)
            {
                var obj = Instantiate(loginPanelPrefab, loginPanelParent);
                LoginPanel panel = obj.GetComponent<LoginPanel>();
                if (panel)
                {
                    panel.SetGameDataManager(dataManager);
                    panel.SetLoginManager(this);
                    panel.SetSlotInd(i);
                    panel.LoadNewPlayerWindow(true);

                    loadedPanels.Add(panel);
                }
            }
        }

        if (players.players.Count > 0)
        {
            foreach (var panel in loadedPanels)
            {
                PlayerData player;
                if (dataManager.PlayerExists(panel.GetSlotInd(), out player))
                {
                    panel.LoadNewPlayerWindow(false);
                    panel.SetTextData(player.playerName, player.coins,
                        player.lives, player.levelUnlocked);
                }
                else
                {
                    panel.LoadNewPlayerWindow(true);
                }
              
            }

        }
        else
        {
            LoadAllNewPlayerWindows();
        }
    }

    void LoadAllNewPlayerWindows()
    {
        foreach (var panel in loadedPanels)
        {

                panel.LoadNewPlayerWindow(true);
            
        }
    }

    public string GetNextSceneToPlay()
    {
        return nextSceneToPlay;
    }

    public GameObject GetLoginPanelParent()
    {
        return loginPanelParent.gameObject;
    }

}
