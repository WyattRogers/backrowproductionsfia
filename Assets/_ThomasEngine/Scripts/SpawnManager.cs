﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour 
{

    [SerializeField]
    private Transform[] checkPoints;
    [SerializeField]
    private bool progressOnly = true;
    [SerializeField]
    private bool resetLevelOnDeath = false;
    [SerializeField]
    private bool saveProgressToDisc = true;
    [SerializeField]
    private bool resetProgressOnQuit = true;
    [SerializeField]
    private bool resetProgressOnStart = false;
    [SerializeField]
    private GameObject playerSpawn;
    [SerializeField]
    private float respawnTime = 3;
    
    private int curCheckPoint;
    private Transform player;

    private LevelManager lm;
    private GameDataManager dataManager;

    // Use this for initialization
    void Start () 
	{
        lm = GameManager.instance.GetLevelManager();
        dataManager = GameManager.instance.GetGameDataManager();

        SetCheckPointIndexes();

        StartCoroutine(StartWaitForData());
	}
	
    IEnumerator StartWaitForData()
    {
        while (dataManager.GetCurPlayer() == null)
        {
            yield return new WaitForEndOfFrame();
        }
        if (resetProgressOnStart)
        {
            ResetAllProgress();
            yield return new WaitForEndOfFrame();
        }
        BeginSpawn();
    }

    void BeginSpawn()
    {
        SpawnPlayer();
    }

    void SetCheckPointIndexes()
    {
        for (int i = 0; i < checkPoints.Length; i++)
        {
            checkPoints[i].GetComponent<CheckPoint>().SetCheckPointInd(i);
        }
    }

	public void SpawnPlayer()
    {
        string savedName = dataManager.GetCurSavedLevelName();
        if (savedName == lm.GetCurLevelName())
        {
            curCheckPoint = dataManager.GetCurSavedCheckPoint();
            Debug.Log("Saved level name matches current level...spawning player at checkpoint " + curCheckPoint);
        }
        else
        {
            Debug.Log("Saved level name does not match current level name...Resetting checkpoint to 0");
            curCheckPoint = 0;
        }
        
        //store spawn in transform var for respawn use
        player = Instantiate(playerSpawn, checkPoints[curCheckPoint].position, 
            checkPoints[curCheckPoint].rotation).transform;
    }

    public void RespawnPlayer()
    {
        StartCoroutine(StartRespawnPlayer());
    }

    public IEnumerator StartRespawnPlayer()
    {
        yield return new WaitForSeconds(respawnTime);

        if (dataManager.GetCurLives() > 0)
        {
            if (resetLevelOnDeath)
                //reset level at checkpoint 
                ResetLevel(curCheckPoint);
            else
                ResetPlayerPosition();
        }
        else
            //reset entire level at first checkpoint if no lives
            ResetLevel(0);      
    }

    void ResetPlayerPosition()
    {
        Debug.Log("resetting player position to " + checkPoints[curCheckPoint].position);
        player.position = checkPoints[curCheckPoint].position;

        //reset player health
        Player health = player.GetComponent<Player>();
        health.SetupPlayer();
    }

    void ResetLevel(int _checkPoint)
    {
        dataManager.SetCurCheckPoint(_checkPoint);
        dataManager.SetCurLevelName(lm.GetCurLevelName());
        lm.ResetCurLevel();
    }

    public void SetCurCheckPoint(int _ind)
    {
        if (progressOnly)
        {
            if (_ind > curCheckPoint)
                curCheckPoint = _ind;
        }
        else
            curCheckPoint = _ind;

        if (saveProgressToDisc)
        {
            dataManager.SetCurCheckPoint(curCheckPoint);
            dataManager.SetCurLevelName(lm.GetCurLevelName());
        }
    }

    void ResetAllProgress()
    {
        Debug.Log("Erasing Checkpoint progress for " + lm.GetCurLevelName());
        dataManager.ResetCurCheckPoint();
        dataManager.EraseSavedLevelName();
    }

    void OnApplicationQuit()
    {
        if (resetProgressOnQuit)
        {
            ResetAllProgress();
        }
        else
        {
            Debug.Log("Keeping Checkpoint progress for " + lm.GetCurLevelName());
            dataManager.SetCurLevelName(lm.GetCurLevelName());
            dataManager.SetCurCheckPoint(curCheckPoint);
        }
    }

}
