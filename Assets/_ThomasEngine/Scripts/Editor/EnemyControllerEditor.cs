﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(EnemyController))]
public class EnemyControllerEditor : Editor
{

    private EnemyController source;
    private SerializedObject sourceRef;

    private SerializedProperty faceRightAtStart;
    //movement
    private SerializedProperty movementType;
    private SerializedProperty speed;
    //boundaries
    private SerializedProperty useBoundaries;
    private SerializedProperty leftPos;
    private SerializedProperty rightPos;
    //collision hits
    private SerializedProperty flipOnCollision;
    private SerializedProperty collisionMask;
    private SerializedProperty leftDetectSize;
    private SerializedProperty leftDetectCenter;
    private SerializedProperty rightDetectSize;
    private SerializedProperty rightDetectCenter;
    //bounce
    private SerializedProperty bounce;
    private SerializedProperty bouncePower;
    private SerializedProperty bounceXAddedForce;
    private SerializedProperty bounceDelay;
    //ground
    private SerializedProperty groundMask;
    private SerializedProperty groundBoxSize;
    private SerializedProperty groundBoxCenter;

    private Vector2 handleLeft;
    private Vector2 handleRight;

    private Bounds col;

    private bool animFoldout;

    private void OnEnable()
    {
        source = (EnemyController)target;
        sourceRef = serializedObject;

        GetProperties();
    }

    public override void OnInspectorGUI()
    {
        SetProperties();

        sourceRef.ApplyModifiedProperties();
    }

    void GetProperties()
    {
        faceRightAtStart = sourceRef.FindProperty("faceRightAtStart");
        //movement
        movementType = sourceRef.FindProperty("movementType");
        useBoundaries = sourceRef.FindProperty("useBoundaries");
        speed = sourceRef.FindProperty("speed");
        leftPos = sourceRef.FindProperty("leftPos");
        rightPos = sourceRef.FindProperty("rightPos");
        //collision hits
        flipOnCollision = sourceRef.FindProperty("flipOnCollision");
        collisionMask = sourceRef.FindProperty("collisionMask");
        leftDetectSize = sourceRef.FindProperty("leftDetectSize");
        leftDetectCenter = sourceRef.FindProperty("leftDetectCenter");
        rightDetectSize = sourceRef.FindProperty("rightDetectSize");
        rightDetectCenter = sourceRef.FindProperty("rightDetectCenter");
        //bounce
        bounce = sourceRef.FindProperty("bounce");
        bouncePower = sourceRef.FindProperty("bouncePower");
        bounceXAddedForce = sourceRef.FindProperty("bounceXAddedForce");
        bounceDelay = sourceRef.FindProperty("bounceDelay");
        //ground
        groundMask = sourceRef.FindProperty("groundMask");
        groundBoxSize = sourceRef.FindProperty("groundBoxSize");
        groundBoxCenter = sourceRef.FindProperty("groundBoxCenter");

    }

    void SetGroundBox()
    {
        if (groundBoxSize.vector2Value == Vector2.zero)
        {
            col = source.GetComponent<Collider2D>().bounds;
            Vector3 pos = source.transform.position;
            float sizeX = col.size.x;
            float sizeY = source.transform.InverseTransformPoint(pos - col.extents).y; 
            source.groundBoxSize = new Vector2(sizeX,0.15f);
            source.groundBoxCenter = new Vector2(0, sizeY);
        }
    }

    void SetProperties()
    {
        if (Selection.activeGameObject != source.gameObject)
            return;

        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(faceRightAtStart);
        //movement
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(movementType);
        if (movementType.enumValueIndex == 1)
        {
            EditorGUILayout.PropertyField(speed);
        }
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(useBoundaries);
        if (useBoundaries.boolValue)
        {
            EditorGUILayout.PropertyField(leftPos);
            EditorGUILayout.PropertyField(rightPos);
        }
        //collision hits
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(flipOnCollision);
        if (flipOnCollision.boolValue)
        {
            EditorGUILayout.PropertyField(collisionMask);
            EditorGUILayout.PropertyField(leftDetectSize);
            EditorGUILayout.PropertyField(leftDetectCenter);
            EditorGUILayout.PropertyField(rightDetectSize);
            EditorGUILayout.PropertyField(rightDetectCenter);
        }
        //bounce
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(bounce);
        if (bounce.boolValue)
        {
            EditorGUILayout.PropertyField(bouncePower);
            EditorGUILayout.PropertyField(bounceDelay);
            if (movementType.enumValueIndex == 0)
            {
                EditorGUILayout.PropertyField(bounceXAddedForce);
            }
                
        }
        //ground
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Grounding Properties");
        EditorGUILayout.PropertyField(groundMask);
        EditorGUILayout.PropertyField(groundBoxSize);
        EditorGUILayout.PropertyField(groundBoxCenter);

    }

    public void OnSceneGUI()
    {
        if (Selection.activeGameObject != source.gameObject)
            return;

        if (useBoundaries.boolValue)
            DrawPositionHandles();

        //draw grounded gizmo
        Handles.color = Color.cyan;
        Handles.DrawWireCube((Vector2)source.transform.position + groundBoxCenter.vector2Value,
            groundBoxSize.vector2Value);

        //draw collision gizmos
        if (flipOnCollision.boolValue)
        {
            Handles.color = Color.magenta;
            Handles.DrawWireCube((Vector2)source.transform.position + leftDetectCenter.vector2Value,
                leftDetectSize.vector2Value);
            Handles.DrawWireCube((Vector2)source.transform.position + rightDetectCenter.vector2Value,
                rightDetectSize.vector2Value);
        }

    }

    void DrawPositionHandles()
    {

        InitializeHandlePositions();
        SetGroundBox();

        EditorGUI.BeginChangeCheck();

        if (handleLeft != leftPos.vector2Value)
        {
            handleLeft = Handles.PositionHandle(source.leftPos, Quaternion.identity);
        }
        else
            handleLeft = Handles.PositionHandle(handleLeft, Quaternion.identity);

        if (handleRight != rightPos.vector2Value)
        {
            handleRight = Handles.PositionHandle(source.rightPos, Quaternion.identity);
        }
        else
            handleRight = Handles.PositionHandle(handleRight, Quaternion.identity);

        if (EditorGUI.EndChangeCheck())//update script values after dragging
        {
            Undo.RecordObject(source, "Modified " + source + " properties.");
            if (handleLeft.x > source.transform.position.x)
                handleLeft.x = source.transform.position.x - 1;
            if (handleRight.x < source.transform.position.x)
                handleRight.x = source.transform.position.x + 1;

            source.leftPos = new Vector2(handleLeft.x, source.transform.position.y);
            source.rightPos = new Vector2(handleRight.x, source.transform.position.y);
        }

        //draw icons
        DrawSideLines(handleLeft, "Left Pos");
        DrawSideLines(handleRight, "Right Pos");

    }

    void InitializeHandlePositions()
    {
        //set Initial positions for other points so they don't all start at zero zero
        if (leftPos.vector2Value == Vector2.zero)
        {
            Vector2 pos = source.transform.position;
            source.leftPos = new Vector2(pos.x - 3, pos.y);
        }
        if (rightPos.vector2Value == Vector2.zero)
        {
            Vector2 pos = source.transform.position;
            source.rightPos = new Vector2(pos.x + 3, pos.y);
        }
    }

    void DrawSideLines(Vector2 _pos, string _labelName)
    {
        Handles.Label(_pos, _labelName);
        Handles.DrawSolidDisc(_pos, Vector3.back, 0.3f);
        Handles.DrawDottedLine(new Vector2(_pos.x, _pos.y - 5), new Vector2(_pos.x, _pos.y + 5), 5);
    }

}
