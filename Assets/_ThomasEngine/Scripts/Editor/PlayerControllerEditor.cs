﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerController))]
public class PlayerControllerEditor : Editor
{
    private PlayerController source;
    private GUIStyle headerStyle = new GUIStyle();

    private SerializedObject sourceRef;

    //Movement Properties
    private SerializedProperty faceRightAtStart;
    private SerializedProperty speed;
    private SerializedProperty enableRun;
    private SerializedProperty runSpeed;
    private SerializedProperty enableCrouch;
    private SerializedProperty crouchSpeed;
    private SerializedProperty crouchSpeedTime;
    private SerializedProperty inAirControlTime;
    private SerializedProperty enableClimbing;
    private SerializedProperty climbSpeed;

    //Jumping Properties
    private SerializedProperty jumpStyle;
    private SerializedProperty jumpPower;
    private SerializedProperty enableDoubleJump;
    private SerializedProperty enableJumpClimbing;
    private SerializedProperty gravityMultiplier;
    private SerializedProperty lowJumpMultiplier;

    //Ground Detection
    private SerializedProperty groundMask;
    private SerializedProperty groundBoxSize;
    private SerializedProperty groundBoxCenter;

    //ceiling Detection
    private SerializedProperty ceilingMask;
    private SerializedProperty ceilingBoxSize;
    private SerializedProperty ceilingBoxCenter;
    private SerializedProperty curCeilingBoxCenter;

    //Wall Detection
    private SerializedProperty enableWallJump;
    private SerializedProperty wallMask;
    private SerializedProperty leftDetectSize;
    private SerializedProperty leftDetectCenter;
    private SerializedProperty rightDetectSize;
    private SerializedProperty rightDetectCenter;
    private SerializedProperty autoFlip;
    private SerializedProperty wallBounceForceX;
    private SerializedProperty wallBounceForceY;
    private SerializedProperty disableMovementOnWallJump;
    private SerializedProperty disableTime;

    protected static bool showMovement = true;
    protected static bool showJumping = true;
    protected static bool showGroundDetection = true;
    protected static bool showCeilingDetection = true;
    protected static bool showWallDetection = true;

    private Bounds col;

    private void OnEnable()
    {
        source = (PlayerController)target;
        sourceRef = serializedObject;

        SetHeaderStyle();

        GetMovementProperties();
        GetJumpingProperties();
        GetGroundDetectionProperties();
        GetCeilingDetectionProperties();
        GetWallDetectionProperties();
    }

    public override void OnInspectorGUI()
    {
        SetMovementProperties();
        SetJumpingProperties();
        SetGroundDetectionProperties();
        SetCeilingDetectionProperties();
        SetWallDetectionProperties();

        sourceRef.ApplyModifiedProperties();
    }

    void SetHeaderStyle()
    {
        headerStyle.fontStyle = FontStyle.Bold;
        RectOffset paddingOffset = new RectOffset(0, 0, 3, 3);
        headerStyle.padding = paddingOffset;
    }

    void SetGroundBox()
    {
        if (groundBoxSize.vector2Value == Vector2.zero)
        {
            col = source.GetComponent<Collider2D>().bounds;
            Vector3 pos = source.transform.position;
            float sizeX = col.size.x;
            source.groundBoxSize = new Vector2(sizeX, 0.15f);
        }
    }

    void SetCeilingBox()
    {
        if (ceilingBoxSize.vector2Value == Vector2.zero)
        {
            col = source.GetComponent<Collider2D>().bounds;
            Vector3 pos = source.transform.position;
            float sizeX = col.size.x;
            float sizeY = source.transform.InverseTransformPoint(pos + col.extents).y;
            source.ceilingBoxSize = new Vector2(sizeX, 0.15f);
            source.groundBoxCenter = new Vector2(0, sizeY);
        }
    }

    void GetMovementProperties()
    {
        faceRightAtStart = sourceRef.FindProperty("faceRightAtStart");
        speed = sourceRef.FindProperty("speed");
        enableRun = sourceRef.FindProperty("enableRun");
        runSpeed = sourceRef.FindProperty("runSpeed");
        enableCrouch = sourceRef.FindProperty("enableCrouch");
        crouchSpeed = sourceRef.FindProperty("crouchSpeed");
        crouchSpeedTime = sourceRef.FindProperty("crouchSpeedTime");
        inAirControlTime = sourceRef.FindProperty("inAirControlTime");
        enableClimbing = sourceRef.FindProperty("enableClimbing");
        climbSpeed = sourceRef.FindProperty("climbSpeed");
    }

    void GetJumpingProperties()
    {
        jumpStyle = sourceRef.FindProperty("jumpStyle");
        jumpPower = sourceRef.FindProperty("jumpPower");
        enableDoubleJump = sourceRef.FindProperty("enableDoubleJump");
        gravityMultiplier = sourceRef.FindProperty("gravityMultiplier");
        lowJumpMultiplier = sourceRef.FindProperty("lowJumpMultiplier");
        lowJumpMultiplier = sourceRef.FindProperty("lowJumpMultiplier");
        enableJumpClimbing = sourceRef.FindProperty("enableJumpClimbing");
    }

    void GetGroundDetectionProperties()
    {
        groundMask = sourceRef.FindProperty("groundMask");
        groundBoxSize = sourceRef.FindProperty("groundBoxSize");
        groundBoxCenter = sourceRef.FindProperty("groundBoxCenter");
    }

    void GetCeilingDetectionProperties()
    {
        ceilingMask = sourceRef.FindProperty("ceilingMask");
        ceilingBoxSize = sourceRef.FindProperty("ceilingBoxSize");
        ceilingBoxCenter = sourceRef.FindProperty("ceilingBoxCenter");
        curCeilingBoxCenter = sourceRef.FindProperty("curCeilingBoxCenter");
    }

    void GetWallDetectionProperties()
    {
        enableWallJump = sourceRef.FindProperty("enableWallJump");
        wallMask = sourceRef.FindProperty("wallMask");
        leftDetectSize = sourceRef.FindProperty("leftDetectSize");
        leftDetectCenter = sourceRef.FindProperty("leftDetectCenter");
        rightDetectSize = sourceRef.FindProperty("rightDetectSize");
        rightDetectCenter = sourceRef.FindProperty("rightDetectCenter");
        autoFlip = sourceRef.FindProperty("autoFlip");
        wallBounceForceX = sourceRef.FindProperty("wallBounceForceX");
        wallBounceForceY = sourceRef.FindProperty("wallBounceForceY");
        disableMovementOnWallJump = sourceRef.FindProperty("disableMovementOnWallJump");
        disableTime = sourceRef.FindProperty("disableTime");
    }

    void SetMovementProperties()
    {
        EditorGUILayout.Space();
        showMovement = EditorGUILayout.Foldout(showMovement,"Movement");
        if (showMovement)
        {
            EditorGUILayout.PropertyField(faceRightAtStart);
            EditorGUILayout.PropertyField(speed);
            EditorGUILayout.PropertyField(enableRun);
            if (enableRun.boolValue)
                EditorGUILayout.PropertyField(runSpeed);
            EditorGUILayout.PropertyField(enableCrouch);
            if (enableCrouch.boolValue)
            {
                EditorGUILayout.PropertyField(crouchSpeed);
                EditorGUILayout.PropertyField(crouchSpeedTime);
            }
            EditorGUILayout.PropertyField(enableClimbing);
            if (enableClimbing.boolValue)
                EditorGUILayout.PropertyField(climbSpeed);
            EditorGUILayout.PropertyField(inAirControlTime);
        } 
    }

    void SetJumpingProperties()
    {
        EditorGUILayout.Space();
        showJumping = EditorGUILayout.Foldout(showJumping, "Jumping");
        if (showJumping)
        {
            EditorGUILayout.PropertyField(jumpStyle);
            EditorGUILayout.PropertyField(jumpPower);
            EditorGUILayout.PropertyField(enableDoubleJump);
            if (enableClimbing.boolValue)
            {
                EditorGUILayout.PropertyField(enableJumpClimbing);
            }
            EditorGUILayout.PropertyField(enableWallJump);
            if (enableWallJump.boolValue)
            {
                EditorGUILayout.PropertyField(wallBounceForceX);
                EditorGUILayout.PropertyField(wallBounceForceY);
                EditorGUILayout.PropertyField(disableMovementOnWallJump);
                if (disableMovementOnWallJump.boolValue)
                    EditorGUILayout.PropertyField(disableTime);
            }
            if (jumpStyle.enumValueIndex == 1)
            {
                EditorGUILayout.PropertyField(gravityMultiplier);
                EditorGUILayout.PropertyField(lowJumpMultiplier);
            }
        }
    }

    void SetGroundDetectionProperties()
    {
        EditorGUILayout.Space();
        showGroundDetection = EditorGUILayout.Foldout(showGroundDetection, "Ground Detection");
        if (showGroundDetection)
        {
            EditorGUILayout.PropertyField(groundMask);
            EditorGUILayout.PropertyField(groundBoxSize);
            EditorGUILayout.PropertyField(groundBoxCenter);
        }

    }

    void SetCeilingDetectionProperties()
    {
        EditorGUILayout.Space();
        showCeilingDetection = EditorGUILayout.Foldout(showCeilingDetection, "Ceiling Detection");
        if (showGroundDetection)
        {
            EditorGUILayout.PropertyField(ceilingMask);
            EditorGUILayout.PropertyField(ceilingBoxSize);
            EditorGUILayout.PropertyField(ceilingBoxCenter);
        }
    }

    void SetWallDetectionProperties()
    {
        EditorGUILayout.Space();
        showWallDetection = EditorGUILayout.Foldout(showWallDetection, "Wall Detection");

        if (showWallDetection)
        {
            EditorGUILayout.PropertyField(autoFlip);
            EditorGUILayout.PropertyField(wallMask);
            EditorGUILayout.PropertyField(leftDetectSize);
            EditorGUILayout.PropertyField(leftDetectCenter);
            EditorGUILayout.PropertyField(rightDetectSize);
            EditorGUILayout.PropertyField(rightDetectCenter);
        }

    }

    public void OnSceneGUI()
    {
        if (Selection.activeGameObject != source.gameObject)
            return;

        SetGroundBox();
        SetCeilingBox();

        //draw grounded gizmo
        Handles.color = Color.cyan;
        Handles.DrawWireCube((Vector2)source.transform.position + groundBoxCenter.vector2Value,
            groundBoxSize.vector2Value);

        //draw ceiling gizmo
        Handles.color = Color.green;
        Vector2 center = ceilingBoxCenter.vector2Value;
        if (Application.isPlaying)
            center = curCeilingBoxCenter.vector2Value;
        Handles.DrawWireCube((Vector2)source.transform.position + center,
            ceilingBoxSize.vector2Value);

        //draw wall gizmos
        Handles.color = Color.magenta;
        Handles.DrawWireCube((Vector2)source.transform.position + leftDetectCenter.vector2Value,
            leftDetectSize.vector2Value);
        Handles.DrawWireCube((Vector2)source.transform.position + rightDetectCenter.vector2Value,
            rightDetectSize.vector2Value);
    }
}
