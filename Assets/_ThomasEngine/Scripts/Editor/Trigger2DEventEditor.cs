﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Trigger2DEvent))]
public class Trigger2DEventEditor : Editor
{
    private Trigger2DEvent source;
    private SerializedObject sourceRef;
    private SerializedProperty trigger2DEnter;
    private SerializedProperty trigger2DExit;
    private SerializedProperty trigger2DStay;

    private void OnEnable()
    {
        source = (Trigger2DEvent)target;
        sourceRef = serializedObject;

        trigger2DEnter = sourceRef.FindProperty("trigger2DEnterEvents");
        trigger2DExit = sourceRef.FindProperty("trigger2DExitEvents");
        trigger2DStay = sourceRef.FindProperty("trigger2DStayEvents");
    }

    public override void OnInspectorGUI()
    {
        source.triggerTag = EditorGUILayout.TagField("Trigger2D Tag", source.triggerTag);
        source.mask = EditorGUILayout.MaskField("Trigger2D Types", source.mask, source.maskOptions);
        int i = source.mask;

        if (i == 1 | i == 3 | i == 5 | i == -1)
        {
            EditorGUILayout.PropertyField(trigger2DEnter);
        }


        if (i == 2 | i == 3 | i == 6 | i == -1)
        {
            EditorGUILayout.PropertyField(trigger2DExit);
        }


        if (i == 4 | i == 6 | i == 5 | i == -1)
        {
            EditorGUILayout.PropertyField(trigger2DStay);
        }


        sourceRef.ApplyModifiedProperties();
    }
	
}
