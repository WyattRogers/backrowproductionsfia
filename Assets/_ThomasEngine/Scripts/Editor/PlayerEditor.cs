﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(Player))]
public class PlayerEditor : Editor
{

    private SerializedObject sourceRef;
    //health
    private SerializedProperty maxHp;
    private SerializedProperty maxLives;
    private SerializedProperty allowGameOver;
    //enemy configs
    private SerializedProperty enemyTarget;
    //skins
    private SerializedProperty skins;
    private SerializedProperty setSkin;
    private SerializedProperty skinInd;
    //physics ignore
    private SerializedProperty ignorePhysicsOnHit;
    private SerializedProperty ignoreLayerOne;
    private SerializedProperty ignoreLayerTwo;
    private SerializedProperty ignoreTime;
    private SerializedProperty stunTime;
    //material swaps
    private SerializedProperty changeMeshMaterialOnHit;
    private SerializedProperty materialToUse;


    private void OnEnable()
    {
        sourceRef = serializedObject;

        GetProperties();
    }

    public override void OnInspectorGUI()
    {

        SetProperties();

        sourceRef.ApplyModifiedProperties();
    }

    void GetProperties()
    {
        //health
        maxHp = sourceRef.FindProperty("maxHp");
        maxLives = sourceRef.FindProperty("maxLives");
        allowGameOver = sourceRef.FindProperty("allowGameOver");
        //enemy
        enemyTarget = sourceRef.FindProperty("enemyTarget");
        //skins
        skins = sourceRef.FindProperty("skins");
        setSkin = sourceRef.FindProperty("setSkin");
        skinInd = sourceRef.FindProperty("skinInd");
        //ignore physics
        ignorePhysicsOnHit = sourceRef.FindProperty("ignorePhysicsOnHit");
        ignoreLayerOne = sourceRef.FindProperty("ignoreLayerOne");
        ignoreLayerTwo = sourceRef.FindProperty("ignoreLayerTwo");
        ignoreTime = sourceRef.FindProperty("ignoreTime");
        stunTime = sourceRef.FindProperty("stunTime");
        //material change
        changeMeshMaterialOnHit = sourceRef.FindProperty("changeMeshMaterialOnHit");
        materialToUse = sourceRef.FindProperty("materialToUse");
    }

    void SetProperties()
    {
        EditorGUILayout.Space();
        //health
        EditorGUILayout.PropertyField(maxHp);
        EditorGUILayout.PropertyField(maxLives);
        EditorGUILayout.PropertyField(allowGameOver);
        //enemy
        EditorGUILayout.PropertyField(enemyTarget);
        //skins
        EditorGUILayout.PropertyField(skins, true);
        EditorGUILayout.PropertyField(setSkin);
        if (setSkin.boolValue)
            EditorGUILayout.PropertyField(skinInd);
        //ignore physics
        EditorGUILayout.PropertyField(ignorePhysicsOnHit);
        if (ignorePhysicsOnHit.boolValue)
        {
            ignoreLayerOne.intValue = EditorGUILayout.LayerField("Ignore Layer One", ignoreLayerOne.intValue);
            ignoreLayerTwo.intValue = EditorGUILayout.LayerField("Ignore Layer Two", ignoreLayerTwo.intValue);
            EditorGUILayout.PropertyField(ignoreTime);
        }
        EditorGUILayout.PropertyField(stunTime);
        //material change
        EditorGUILayout.PropertyField(changeMeshMaterialOnHit);
        if(changeMeshMaterialOnHit.boolValue)
            EditorGUILayout.PropertyField(materialToUse);

    }

}
